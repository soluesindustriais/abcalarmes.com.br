<?
$h1         = 'ABC Alarmes';
$title      = 'Início';
$desc       = 'ABC Alarmes - Aqui você encontra os melhores fornecedores de Alarme residencial! Faça uma cotação e conheça a melhor forma de comprar produtos online. É gratis!';
$var        = 'Home';
include('inc/head.php');
include('inc/fancy.php');
?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1>ALARMES</h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>Alarme Residencial</h2>
        <p>Desenvolvido para aumentar a segurança do local, o alarme residencial pode ser instalado em residências, condomínios, empresas, comércios e muitos outros locais. Existem diversas opções no mercado, todos se adequando da melhor forma ao ambiente em que será instalado.</p>
        <a href="<?=$url?>alarme-residencial" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Alarme de Incêndio</h2>
        <p>O alarme de incêndio é responsável por detectar e sinalizar situações de alerta de fogo em edificações. Por ser um dispositivo que pode conter dimensões diversificadas e ser fabricado em materiais diferentes, a escolha pode ser feita de acordo com cada necessidade apresentada.</p>
        <a href="<?=$url?>alarme-de-incendio" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Central de Alarme</h2>
        <p>É um sistema de funcionamento simplificado onde são instalados detectores de fumaça que são interligados a uma central de alarme. Os detectores são sensíveis a fumaça e quando há indicação no local, o comando aciona a central de alarme...</p>
        <a href="<?=$url?>central-de-alarme" class="cd-btn">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main>
  <section class="wrapper-main">
    <div class="main-center">
      <div class=" quadro-2 ">
        <h2>SISTEMA DE ALARME</h2>
        <div class="div-img">
          <p data-anime="left-0">Toda empresa necessita de um planejamento adequado para prevenir possíveis interrupções e imprevistos, por essa razão é sempre importante contar com uma equipe especializada em solucionar problemas, assim esta tarefa se torna mais corriqueira e menos impactante na linha de produção industrial, esse processo é chamado de manutenção preventiva.</p>
        </div>
        <div class="gerador-svg" data-anime="in">
          <img src="imagens/img-home/alarmes.png" alt="Alarmes" title="Alarmes">
        </div>
      </div>
      <div class=" incomplete-box">
        <ul data-anime="in">
          <li>
            <p>No caso da manutenção preventiva torre de resfriamento, por exemplo, a manutenção preventiva auxilia em uma série de fatores, como por exemplo:</p>
            
            <li><i class="fas fa-angle-right"></i>Reduzir riscos de quebra;</li>
            <li><i class="fas fa-angle-right"></i>Prevenir o envelhecimento e degeneração dos equipamentos;</li>
            <li><i class="fas fa-angle-right"></i>Programar a conversação das peças;
            </li>
            <li><i class="fas fa-angle-right"></i>Amenizar os custos de compra de novos itens.
            </li>
          </ul>
          <a href="<?=$url?>sistema-de-alarme-de-incendio" class="btn-4">Saiba mais</a>
        </div>
      </div>
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-user-shield fa-7x"></i>
            <div>
              <p>SEGURANÇA COMPLETA</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-fire fa-7x"></i>
            <div>
              <p>SISTEMA PARA INCÊNDIO</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-home fa-7x"></i>
            <div>
              <p>ALARME RESIDENCIAL</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-cogs fa-7x"></i>
            <div>
              <p>SERVIÇO DE MANUTENÇÃO</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-user-secret"></i>
            <div>
              <p>MONITORAMENTO 24hrs</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-fire-extinguisher fa-7x"></i>
            <div>
              <p>ALARME PARA INCÊNDIO</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
      <div class="content-icons">
        <div class="produtos-relacionados-1">
          <figure>
            <div class="fig-img2">
              <a href="<?=$url?>alarme-residencial">
                <h2>ALARME RESIDENCIAL</h2>
              </a>
              <a href="<?=$url?>alarme-residencial">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
              </a>
            </div>
          </figure>
        </div>
        <div class="produtos-relacionados-2">
          <figure class="figure2">
            <div class="fig-img2">
              <a href="<?=$url?>central-de-alarme-detector-de-fumaca">
                <h2>CENTRAL DE ALARME DETECTOR DE FUMAÇA</h2>
              </a>
              <a href="<?=$url?>central-de-alarme-detector-de-fumaca">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
              </a>
            </div>
          </figure>
        </div>
        <div class="produtos-relacionados-3">
          <figure>
            <div class="fig-img2">
              <a href="<?=$url?>monitoramento-de-alarmes">
                <h2>MONITORAMENTO DE ALARMES</h2>
              </a>
              <a href="<?=$url?>monitoramento-de-alarmes">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
              </a>
            </div>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/galeria-central-de-alarme-dvr.jpg" class="lightbox" title="Central de alarme dvr">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-central-de-alarme-dvr.jpg" title="Central de alarme dvr" alt="Central de alarme dvr">
            </a>
          </li>
          <li><a href="<?=$url?>imagens/img-home/galeria-conserto-de-sistemas-de-alarmes.jpg" class="lightbox"  title="Conserto de sistemas de alarmes">
            <img src="<?=$url?>imagens/img-home/thumbs/galeria-conserto-de-sistemas-de-alarmes.jpg" alt="Conserto de sistemas de alarmes" title="Conserto de sistemas de alarmes">
          </a>
        </li>
        <li><a href="<?=$url?>imagens/img-home/galeria-alarme-para-banheiro-de-deficiente.jpg" class="lightbox" title="Alarme para banheiro de deficiênte">
          <img src="<?=$url?>imagens/img-home/thumbs/galeria-alarme-para-banheiro-de-deficiente.jpg" alt="Alarme para banheiro de deficiênte" title="Alarme para banheiro de deficiênte">
        </a>
      </li>
      <li><a href="<?=$url?>imagens/img-home/galeria-alarme-de-incendio.jpg" class="lightbox" title="Alarme de incêndio">
        <img src="<?=$url?>imagens/img-home/thumbs/galeria-alarme-de-incendio.jpg" alt="Alarme de incêndio" title="Alarme de incêndio">
      </a>
    </li>
    <li><a href="<?=$url?>imagens/img-home/galeria-central-de-alarme-detector-de-fumaca.jpg" class="lightbox" title="Central de alarme detector de fumaça">
      <img src="<?=$url?>imagens/img-home/thumbs/galeria-central-de-alarme-detector-de-fumaca.jpg" alt="Central de alarme detector de fumaça"  title="Central de alarme detector de fumaça">
    </a>
  </li>
  <li><a href="<?=$url?>imagens/img-home/galeria-instalacao-alarme.jpg" class="lightbox" title="Instalação alarme">
    <img src="<?=$url?>imagens/img-home/thumbs/galeria-instalacao-alarme.jpg" alt="Instalação alarme" title="Instalação alarme">
  </a>
</li>
<li><a href="<?=$url?>imagens/img-home/galeria-sistema-de-alarme-e-deteccao-de-incendio.jpg" class="lightbox" title="Sistema de alarme e detecção de incêndio">
  <img src="<?=$url?>imagens/img-home/thumbs/galeria-sistema-de-alarme-e-deteccao-de-incendio.jpg" alt="Sistema de alarme e detecção de incêndio" title="Sistema de alarme e detecção de incêndio">
</a>
</li>
<li><a href="<?=$url?>imagens/img-home/galeria-manutencao-em-sistema-de-alarme-de-incendio.jpg" class="lightbox" title="Manutenção em sistema de alarme de incêndio">
<img src="<?=$url?>imagens/img-home/thumbs/galeria-manutencao-em-sistema-de-alarme-de-incendio.jpg" alt="Manutenção em sistema de alarme de incêndio" title="Manutenção em sistema de alarme de incêndio">
</a>
</li>
<li><a href="<?=$url?>imagens/img-home/galeria-monitoramento-alarme-24h.jpg" class="lightbox" title="Monitoramento alarme 24h">
<img src="<?=$url?>imagens/img-home/thumbs/galeria-monitoramento-alarme-24h.jpg" alt="Monitoramento alarme 24h" title="Monitoramento alarme 24h">
</a>
</li>
<li><a href="<?=$url?>imagens/img-home/galeria-sistema-de-deteccao-e-alarme-de-incendio.jpg" class="lightbox" title="Sistema de detecção e alarme de incêndio">
<img src="<?=$url?>imagens/img-home/thumbs/galeria-sistema-de-deteccao-e-alarme-de-incendio.jpg" alt="Sistema de detecção e alarme de incêndio" title="Sistema de detecção e alarme de incêndio">
</a>
</li>
</ul>
</div>
</div>
</section>
</main>
<? include('inc/footer.php'); ?>
<link rel="stylesheet" href="<?=$url?>nivo/nivo-slider.css" media="screen">
<script  src="<?=$url?>nivo/jquery.nivo.slider.js"></script>
<script >
$(window).load(function() {
$('#slider').nivoSlider();
});
</script>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
</body>
</html>