<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <?php
    $lv = 4;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>
  </div>
  <div class="page-title">
    <div class="title col-md-12 col-sm-6 col-xs-12">
      <h3><i class="fa fa-briefcase"></i> Lista de empresas cadastradas</h3>
    </div>
    <div class="clearfix"></div>
    <?php
    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
    if (isset($get) && $get == true && isset($_SESSION['Error'])):
    //COLOCAR ALERTA PERSONALIZADOS
      WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
    unset($_SESSION['Error']);
  endif;
  $PerPage = filter_input(INPUT_GET, 'perpage', FILTER_VALIDATE_INT);
  $PerPage = (!empty($PerPage) || isset($PerPage) ? $PerPage : 25);
  $Page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);
  $Pager = new Pager("painel.php?exe=CMSemp/index&perpage={$PerPage}&page=");
  $Pager->ExePager($Page, $PerPage);
  $ReadRecursos = new Read;
  $post = filter_input(INPUT_POST, 'busca', FILTER_DEFAULT);
  $search = (!empty($post) ? urldecode(strip_tags(trim($post))) : null);
  ?>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Utilize o campo de pesquisa para filtrar sua busca.<small>Você também pode acessar, editar e alterar status de acesso das empresas.</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
          </ul>
          <div class="clearfix"></div>
          <br class="clearfix" />
          <!--BLOCO COM CAMPO DE PESQUISA E ITENS POR PÁGINA-->
          <?php include('inc/filter-search.inc.php'); ?>
          <!--/BLOCO COM CAMPO DE PESQUISA E ITENS POR PÁGINA-->
          
        </div>
        <?php
        if (!empty($search)):
          $search = strip_tags(trim(urlencode($search)));
          header('Location: ' . BASE . '/painel.php?exe=CMSemp/index&busca=' . $search);
        endif;
        $s = filter_input(INPUT_GET, 'busca', FILTER_DEFAULT);
        $ReadEmps = new Read;
        if (!empty($s)):
          $ReadEmps->ExeRead(TB_EMP, "WHERE empresa_status != :st AND empresa_id != :id AND (empresa_name LIKE '%' :s '%') OR (empresa_razaosocial LIKE '%' :s '%') OR (empresa_site LIKE '%' :s '%')  OR (empresa_id LIKE '%' :s '%') AND empresa_parent IS NULL ORDER BY empresa_name ASC LIMIT :limit OFFSET :offset", "st=3&id=" . EMPRESA_MASTER . "&s={$s}&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
        else:
          $ReadEmps->ExeRead(TB_EMP, "WHERE empresa_status != :st AND empresa_id != :id AND empresa_parent IS NULL ORDER BY empresa_name DESC LIMIT :limit OFFSET :offset", "st=3&id=" . EMPRESA_MASTER . "&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
        endif;
        if (!$ReadEmps->getResult()):
          $Pager->ReturnPage();
          WSErro("Nenhum item foi encontrado em sua pesquisa.", WS_ALERT, null, "MPI Technology");
        else:
          ?>
          <div class="x_content">
            <div class="table-responsive">
              <table class="table nowrap">
                <thead>
                  <tr>
                    <th>Código</th>
                    <th>Logo</th>
                    <th>Site</th>
                    <th>Empresa</th>
                    <th>Ações</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($ReadEmps->getResult() as $key): extract($key); ?>
                    <tr class="j_item" id="<?= $empresa_id; ?>">
                      <th scope="row"><?= $empresa_id; ?></th>
                      <th><?= Check::Image('../doutor/uploads/' . $empresa_capa, $empresa_name, '', 100, 100); ?></th>
                      <td><?= Check::TrataUrl($empresa_site); ?></td>
                      <td><?= $empresa_name; ?></td>
                      <td>
                        <div class="btn-group-xs btn-group pull-right">
                          <button type="button" class="btn btn-dark j_view" id="<?= $empresa_id; ?>" alt="<?= $empresa_name; ?>"><i class="fa fa-eye"></i></button>
                          <!--<button type="button" class="btn btn-info j_note" rel="<?= $empresa_id; ?>" data-nome="<?= $empresa_name; ?>"><i class="fa fa-comment"></i></button>-->
                          <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=CMSemp/update&id=<?= $empresa_id; ?>'"><i class="fa fa-edit"></i></button>
                          <?php if($user_level > 1): ?>
                            <button type="button" class="btn btn-danger j_remove" action="RemoveEmpresaCliente" rel="<?= $empresa_id; ?>"><i class="fa fa-trash"></i></button>
                          <?php endif; ?>
                          <!--Botão de status do projeto-->
                          <?= Check::GetStatusCliente($empresa_status, $empresa_id); ?>
                          <!-- /Botão de status do projeto-->
                        </div>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
              <div class="clearfix"></div>
              <hr>
              <div class="col-sm-12">
                <div class="btn-toolbar pull-right">
                  <?php
                  if (!empty($s)):
                    $Pager->ExePaginator(TB_EMP, "WHERE empresa_status != :st AND empresa_id != :id AND (empresa_name LIKE '%' :s '%') OR (empresa_razaosocial LIKE '%' :s '%') OR (empresa_site LIKE '%' :s '%')  OR (empresa_id LIKE '%' :s '%') AND empresa_parent IS NULL ORDER BY empresa_name ASC LIMIT :limit OFFSET :offset", "st=3&id=" . EMPRESA_MASTER . "&s={$s}&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
                  else:
                    $Pager->ExePaginator(TB_EMP);
                  endif;
                  echo $Pager->getPaginator();
                  ?>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
</div>
<!-- /page content -->
<!--Modal-->
<div class="j_fundoModal none" id="note_control">
  <div class="j_conteudoModal">
    <div class="btn-group pull-right">
      <button type="button" class="btn btn-primary j_clear"><i class="fa fa-file-word-o"></i> Nova</button>
      <button type="button" class="btn btn-danger j_fechar" id="note_control"><i class="fa fa-close"></i> Fechar</button>
    </div>
    <div class="clearfix"></div>
    <form method="post" enctype="multipart/form-data">
      <div class="x_title">
        <h2></h2>
        <div class="clearfix"></div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-12" for="notas_titulo">Título <span class="required">*</span>
        </label>
        <div class="col-md-12">
          <input name="notas_titulo" class="form-control col-md-12" value="<?php
          if (isset($post['notas_titulo'])): echo $post['notas_titulo'];
          endif;
        ?>">
      </div>
    </div>
    <div class="clearfix"></div>
    <br/>
    <div class="form-group">
      <label class="control-label col-md-12" for="notas_msg">Conteúdo da nota <span class="required">*</span>
      </label>
      <div class="col-md-12">
        <textarea name="notas_msg" id="prod_content"  class="form-control col-md-7 col-xs-12" rows="5"><?php
        if (isset($post['notas_msg'])): echo $post['notas_msg'];
        endif;
      ?></textarea>
    </div>
  </div>
  <div class="clearfix"></div>
  <br/>
</form>
</div>
</div>
<!--/Modal-->