<? $h1 = "Central de alarme de incêndio";
$title  = "Central de alarme de incêndio";
$desc = "$h1, receba uma estimativa de preço de, descubra aqui, faça uma cotação online com mais de 200 fábricas de todo o Brasil";
$key  = "Centrais de alarmes de incêndio,Preço de central de alarme de incêndio";
include('inc/head.php');
include('inc/fancy.php'); ?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main>
		<div class="content">
			<section>
				<?=$caminhoinformacoes?><br class="clear" />
				<h1><?=$h1?></h1>
				<article>
					<div class="img-mpi">
						<a href="<?=$url?>imagens/mpi/central-de-alarme-de-incendio-01.jpg" title="<?=$h1?>" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/central-de-alarme-de-incendio-01.jpg" title="<?=$h1?>" alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/central-de-alarme-de-incendio-02.jpg" title="Centrais de alarmes de incêndio" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/central-de-alarme-de-incendio-02.jpg" title="Centrais de alarmes de incêndio" alt="Centrais de alarmes de incêndio"></a><a href="<?=$url?>imagens/mpi/central-de-alarme-de-incendio-03.jpg" title="Preço de central de alarme de incêndio" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/central-de-alarme-de-incendio-03.jpg" title="Preço de central de alarme de incêndio" alt="Preço de central de alarme de incêndio"></a>
					</div>
					<span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span><hr />
					<h2>EQUIPAMENTO DE INCÊNDIO: COMO FUNCIONA A CENTRAL DE ALARME DE INCÊNDIO?</h2>
					<p>De acordo com as normas de combate ao incêndio, é necessário todos os estabelecimentos comerciais e até mesmo residenciais sigam algumas exigências de segurança do imóvel. A norma foi desenvolvida pelo Corpo de Bombeiro, órgão responsável pela segurança e prevenção de incêndio e uma das formas dessa prevenção é feita através de  vistorias e a emissão da certificação com a nomeação  AVCB (Auto de Vistoria do Corpo de Bombeiros).</p>
					<h2>COMO CONSEGUIR APROVAÇÃO DO CORPO DE BOMBEIROS PARA ITENS DE INCÊNDIO?</h2>
					<p>As empresas ou os imóveis residenciais são submetidos às normas no momento da obra, sendo assim, o ideal é o proprietário e os engenheiros responsáveis pela obra entrem em contato com o Corpo de bombeiro a fim da aprovação de um projeto inicial, neste deve conter todas as informações da obra como metragem, tipos de equipamentos, quantidade, especificação das áreas entre inúmeras informações.</p>
					<h2>APROVAÇÃO DO PROJETO</h2>
					<p>O projeto é elaborado por um engenheiro ou arquiteto, o mesmo terá como base as normas já estabelecidas e por isso deve considerar as normas no momento de elaboração do projeto, pensando em como atender essas exigências e ter maior chances de conseguir a aprovação dos bombeiro para finalmente poder executar e dar continuidade à obra.</p>
					<p>Quando o projeto estiver finalizado, o profissional deve procurar um Corpo de  Bombeiro municipal e apresentar a fim de conseguir a aprovação e a emissão do certificado atestando que o projeto atende as exigências e pode ser executado no local.</p>
					<p>Após analisar o projeto existem duas possibilidades, o bombeiro pode reprovar ou aprovar, caso reprovado o engenheiro ou arquiteto deverá fazer e em seguida solicitar uma reentrada e uma nova análise, caso aprovado, poderá ser executado, mas é importante ser instalado exatamente como está discriminado no projeto.</p>
					<h2>EMISSÃO DO CERTIFICADO</h2>
					<p>Quando a obra for finalizada e os itens de segurança forem instalados é necessário que o Corpo de bombeiro seja acionado novamente para a solicitação de uma vistoria a fim de conferir se os equipamentos foram instalados conforme o projeto aprovado inicialmente.</p>
					<p>Se a instalação não estiver correta o bombeiro emite uma notificação com os itens a serem regularizados e estipula um prazo, é importante que a adequação seja feito dentro do prazo e em seguida seja solicitada uma nova vistoria.  Quanto aos casos de aprovação imediata o certificado é emitido e o imóvel fica regular com o órgão, porém os procedimento variam conforme cada município.</p>
					<p>O certificado também pode ter uma data de vencimento, ou seja, dois meses antes de vencer é necessário o proprietário do imóvel providenciar a renovação da documentação junto do órgão. Mas quanto ao vencimento, varia de município, por isso é preciso estar atento a data de validade mencionada no próprio certificado.</p>
					<p>O Corpos de Bombeiros municipais possuem particularidades e procedimentos diferentes, por isso, é importante estar atento, além das normas aos procedimentos, por exemplo, o atendimento de aprovação varia sendo em alguns municípios apenas presenciais e em outros de forma online.</p>
					<h2>QUAIS SÃO OS RISCOS DE NÃO TER A CERTIFICAÇÃO?</h2>
					<p>Se houver alguma ocorrência de risco ou alguma reclamação de clientes o estabelecimento poderá ser fiscalizado por órgãos públicos que imediatamente vão solicitar a certificação do Corpo de bombeiro e caso não esteja regular o local poderá ser lacrado, até ser regularizado ou em alguns casos será emitida uma notificação com um prazo para adequação e emissão do Auto de Vistoria do Corpo de Bombeiros.</p>
					<h2>PARA QUE SERVEM OS EQUIPAMENTOS DE INCÊNDIO?</h2>
					<p>São muitos os itens de segurança para proteção de um imóvel, esses equipamentos têm diversas funções como combater, prevenir e indicar quando um incêndio ocorre. Veja abaixo alguns itens e suas funcionalidades:</p>
					<ul class="list">
						<li><strong>Sprinkler:</strong> Parece com um chuveiro e quando seu sensor é acionado por uma determinada quantidade de fumaça libera água a fim de combater o fogo;</li>
						<li><strong>Central de alarme de incêndio</strong>: São sensores e ficam espalhados, quando acionados por determinada quantidade de fumaça são acionados e emitem um barulho com a intenção de fazer as pessoas evacuarem do ambiente;</li>
						<li><strong>Extintores:</strong> É um cilindro com composições químicas e quando esses gases são submetidos a pressão causada pela válvula do cilindro conseguem combater o incêndio.</li>
						<li><strong>Rota de fuga:</strong> Não é um equipamento, mas trata-se de um trajeto simples e desobstruído para facilitar a evacuação em casos de risco;</li>
						<li><strong>Placas de sinalização:</strong> São diversos modelos e com orientações diferentes, servem para indicar um produto ou um lugar, geralmente são fotoluminescentes para facilitar a leitura quando houver falta de iluminação;</li>
						<li><strong>Hidrantes:</strong> Uma tubulação com mangueira resistente e extensa para a saída de água com maior pressão;</li>
						<li><strong>Porta corta-fogo:</strong> É resistente ao fogo e impede o mesmo e a fumaça passe de um espaço para o outro.</li>
					</ul>
					<p>Além desses equipamentos existem muitos outros que ajudam na proteção do imóvel e das pessoas, são fundamentais para combater e indicar a presença de fumaça, para as pessoas evacuarem rapidamente do local e possam ter maior chance de vida sem a necessidade de um resgate.</p>
					<p>É importante quando uma sirene da <strong>central de alarme de incêndio</strong> for acionada para que as pessoas deixem o local imediatamente e não desconsiderem esse sinal na esperança de acreditar que foi uma falha no sistema, mesmo que seja, o indicado é que evacuação seja feita assim que o suposto incêndio for indicado.</p>
					<h2>COMO FUNCIONA A CENTRAL DE ALARME DE INCÊNDIO?</h2>
					<p>O sistema da <strong>central de alarme de incêndio</strong> é muito funcional e simples. Existe uma central de onde são puxados cabeamentos que ligam a <strong>central de alarme de incêndio</strong> com os detectores de fumaça.</p>
					<p>Os detectores são compostos de sensores esses são sensíveis a fumaça e quando há um situação e risco com emissão de fumaça acionam que emite um som alto para indicar fogo e provocar a evacuação imediata do imóvel. Existem dois sistemas para a <strong>central de alarme de incêndio</strong> o convencional e o sistema endereçável e para entendermos melhor vamos falar sobre a diferença entre eles.</p>
					<h2>SISTEMA CONVENCIONAL DA CENTRAL DE ALARME DE INCÊNDIO</h2>
					<p>Esse sistema é bem funcional e simples, geralmente são usados para áreas menores e com menor número de dispositivo. Nesse sistema os locais onde os detectores estão instalados são identificados por zona e quando acionados é informado na <strong>central de alarme de incêndio</strong> qual a zona em que o suposto incêndio está ocorrendo, porém não consegue informar o dispositivo exato.</p>
					<p>Devido a essa limitação é mais indicado para locais menores o que vai facilitar para identificar qual o ponto, pois para locais maiores corre o risco de perder muito tempo procurando qual o local exato e assim aumentar o fogo e diminuir as chances de combatê-lo.</p>
					<h2>SISTEMA ENDEREÇÁVEL DA CENTRAL DE ALARME DE INCÊNDIO</h2>
					<p>É um sistema mais moderno e permite que cada dispositivo tenha uma numeração específica ligada ao painel e essa funcionalidade é muito interessante, pois vai indicar na central o local exato do suposto incêndio e assim facilitar que o combate ao fogo seja feita de forma mais ágil e assertiva, diminuindo as chances do fogo dominar e causar danos maiores.</p>
					<p>Esse sistema da <strong>central de alarme de incêndio</strong> são subdivididos em duas categorias o que chamamos de categoria A e categoria B, são duas opções diferentes para o mesmo sistema.</p>
					<p><strong>Categoria A:</strong> Cada detector de fumaça possui um cabeamento que inicia no painel e passa por toda alvenaria até chegar no dispositivo. Essa categoria exige o uso de mais cabeamento, mas em contrapartida quando um dispositivo é acionado os demais continuam funcionando sem a interrupção da funcionalidade.</p>
					<p><strong>Categoria B:</strong> Esse sistema possui um único cabeamento e a vantagem é que a economia com o material é maior, ocupando menos espaço por dentro da estrutura até chegar nos dispositivos, porém quando um dispositivo é acionado  funcionalidade dos demais é interrompida.</p>
					<p>Sendo assim, os dois sistemas possuem vantagens e desvantagens, pois enquanto a categoria A não interrompe a funcionalidade dos demais detectores quando um é acionado a Categoria B não promove essa funcionalidade mútua, mas em contrapartida a Categoria B oferece menor custo de material devido usar um único cabeamento.</p>
					<p>O sistema endereçável da central de alarme também é visualmente mais moderno, pois para ambientes comerciais a estética faz toda a diferença. Como as botoeiras são aparentes o ideal é investir em um sistema mais moderno esteticamente principalmente quando os clientes têm acesso ao local.</p>
					<p>Existem também botoeiras que são embutidas e esse procedimento tem sido cada vez mais adotado pelas empresas. O único cuidado é que a instalação precisa ser feita adequadamente e que as botoeiras estejam sensíveis a fumaça sendo ideal que a alvenaria ou outro material de estrutura não interfira nos acessos do sensor.</p>
					<h2>DETECTOR TERMOVELOCIMÉTRICO</h2>
					<p>Esse detector da <strong>central de alarme de incêndio</strong> tem função a fim de acionar quando a temperatura do ambiente é elevada  e estipulado uma temperatura média e quando maior que isso, o alarme é automaticamente acionado. Sua função tem como identificar a chama como princípio de fogo, costuma ser usado em depósitos em locais de materiais inflamáveis.</p>
					<p>A  principal característica que diferencia um detector convencional do termovelocimétrico é que o convencional é acionado com fumaça, já o termovelocimétrico é acionado imediatamente quando há aumento de temperatura.</p>
					<h2>MANUTENÇÃO EM DIA</h2>
					<p>Para qualquer item de incêndio é importante manter a manutenção em dia , pois mesmo que o equipamento e a obra tenha sido aprovada pelo bombeiros e o certificado tenha sido emitido, é importante que sejam sempre revisados por um profissional técnico.</p>
					<p>Existem muitas empresas de manutenção predial que prestam esse serviço e geralmente é feita uma manutenção mensal. A <strong>central de alarme de incêndio</strong> precisa ser constantemente revisada a fim de saber se o sistema está acionado, se os cabeamentos estão promovendo  a interligação entre botoeira e central e se a sirene está ativada provocando disparo.</p>
					<p>A manutenção deve ser feita mensalmente e o técnico usa uma espécie de spray com uma  substância que imita as reações da fumaça e assim o detector é acionado e quando está em perfeito  funcionamento aciona a sirene e automaticamente o painel aponta o local onde o detector foi acionado.</p>
					<h2>ONDE COMPRAR?</h2>
					<p>O sistema da <strong>central de alarme de incêndio</strong> é fácil de encontrar, existem diversos fabricantes, mas é preciso estar atento as referências se o produto tem qualidade, pois é uma responsabilidade muito grande fabricar um produto voltado a segurança e é importante que a possibilidade de falhas sejam inexistentes.</p>
					<p>Existem muitas empresas com variedade de produtos além da <strong>central de alarme de incêndio</strong> e para facilitar essa busca o Soluções Industriais centraliza empresas de produtos voltados a segurança e prevenção de situações de risco.</p>
					<p>É muito importante que antes de comprar o produto a pesquisa por fabricantes seja feita a fim de garantir a compra de um produto de qualidade e com o melhor preço. Muitos fabricantes além de fornecer providenciam também a instalação de algumas empresas oferecem serviço completo desde a fabricação até a instalação e manutenção preventiva.</p>
					<h2>A SEGURANÇA É PRIMORDIAL</h2>
					<p>A instalação  da <strong>central de alarme de incêndio</strong> é muito importante, pois vai influenciar no funcionamento do sistema e é necessário que seja feita da maneira correta para não oferecer futuros problemas como exemplo um falso alarme.</p>
					<p>Além da <strong>central de alarme de incêndio</strong> são muitos outros sistemas que contribuem para o funcionamento e segurança do estabelecimento e permite que tenha maior segurança e menor risco de incêndios.</p>
					<p>É importante que o local tenha bombeiro contratado, esse tipo de contratação tem sido cada vez mais viável e necessária para os estabelecimentos, pois além da manutenção preventiva mensal nos equipamentos é muito relevante um representante técnico diariamente no local e assim garantir ainda mais a segurança do local em perfeito funcionamento dos equipamentos.</p>
					<p>Além disso,  para as empresas é possível também oferecer o curso de brigadista que vai ajudar os colaboradores a saber como agir em situações de risco e conseguir identificar quando alguns equipamentos de incêndio não estiverem em perfeito estado.</p>
					<p>O curso de brigadista ensinará noções básicas sobre os procedimentos e equipamentos de incêndio, mas já contribui muito para a segurança do imóvel e vai capacitar pessoa para que saibam como agir e controlar o estado emocional mediante uma situação de risco.</p>
				</article>
				<? include('inc/coluna-mpi.php');?>
				<br class="clear">
				<? include('inc/busca-mpi.php');?>
				<? include('inc/form-mpi.php');?>
				<? include('inc/regioes.php');?>
			</section>
		</div>
	</main>
</div>
<? include('inc/footer.php');?>
</body>
</html>