<?php
/*------------------------ Instruções ------------------------
 1 - Inserir no inicio da home.php antes do fechamento da tag <head> :
 <style>
  <? include('slick/slick.css'); ?>
 </style>

 2 - Inserir o arquivo carousel
     <? include('inc/produto-home-inc.php'); ?>

 3 - Inserir no fim da home.php depois da include do footer.php
 <script>
    $(document).ready(function(){
       $('.slick-carousel--servicos').slick({
         autoplaySpeed: 3000,
         autoplay: true,
         infinite: true,
         cssEase: 'ease',
         slidesToShow: 3,
         slidesToScroll: 1,
         arrows: true,
         responsive:
         [
         {breakpoint: 767, settings: {slidesToShow: 1}}
         ] 
       });
     });
 </script>
------------------------ Instruções ------------------------ */
?>

<div class="container">
  <div class="wrapper">
    <?php
    $Read->ExeRead(TB_SERVICO, "WHERE user_empresa = :emp AND serv_status = :st ORDER BY serv_date DESC LIMIT 0,4", "emp=" . EMPRESA_CLIENTE . "&st=2");
    if ($Read->getResult()): ?>
      <h2>CONFIRA NOSSOS SERVIÇOS</h2>
      <div class="slick-carousel slick-carousel--servicos">
        <?php
        foreach ($Read->getResult() as $serv_home):
          extract($serv_home); ?>
          <div class="product-card m-5">
            <a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $serv_name; ?>" title="<?= $serv_title; ?>">
              <?= Check::Image('doutor/uploads/' . $serv_cover, $serv_title, 'product-card__image', 300, 300) ?>
              <h2 class="product-card__title"><?= $serv_title; ?></h2>
           </a>
         </div>
       <? endforeach; ?>
     </div>
   <? endif; ?>
 </div>
 <div class="clear"></div>
</div>

