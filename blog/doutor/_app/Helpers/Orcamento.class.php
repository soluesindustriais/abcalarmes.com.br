<?php
/**
* Orcamento.class.php [MODEL]
* Classe responsável por gerir os orçamentos no banco de dados.
* @copyright (c) 2016, Rafael da Silva Lima & Doutores da Web
*/
class Orcamento {
//Tratamento de resultados e mensagens
  private $Result;
  private $Error;
//Entrada de dados
  private $Data;
  private $File;
  private $Total;
//Conexão do FTP
  private $FTP;
/**
* <b>Responsável por cadastrar os dados no banco, a entrada deve ser um array envelopado
* @param array $Data
*/
public function __construct(array $Data) {
  $this->Data = $Data;
  $this->Total = $this->Data['orc_total'];
  unset($this->Data['orc_total']);
  $this->CheckUnset();
  $this->CheckData();
  if ($this->Result):
    $this->Create();
  endif;
}
/**
* <b>Retorno de consulta</b>
* Se não houve consulta ele retorna true boleano ou false para erros
*/
public function getResult() {
  return $this->Result;
}
/**
* <b>Mensagens do sistema</b>
* Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
* @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela.
*/
public function getError() {
  return $this->Error;
}
/**
* <b>Url do anexo</b>
* Retorna a url do anexo para ser enviada por e-mail
* @return string
*/
public function getFile() {
  return $this->File;
}
########################################
########### METODOS PRIVADOS ###########
########################################
//Verifica se algum campo que não é obrigatorio está vazio e remove do array
private function CheckUnset() {
  if (empty($this->Data['orc_anexo'])):
    unset($this->Data['orc_anexo']);
  endif;
  if (empty($this->Data['orc_empresa'])):
    unset($this->Data['orc_empresa']);
  endif;
  if (empty($this->Data['orc_media'])):
    unset($this->Data['orc_media']);
  endif;
  if (empty($this->Data['orc_segmento'])):
    unset($this->Data['orc_segmento']);
  endif;
  if (empty($this->Data['orc_aplicacao'])):
    unset($this->Data['orc_aplicacao']);
  endif;
  if (empty($this->Data['orc_cep'])):
    unset($this->Data['orc_cep']);
  endif;
  if (empty($this->Data['orc_previsao'])):
    unset($this->Data['orc_previsao']);
  endif;
  if (empty($this->Data['orc_mensagem'])):
    unset($this->Data['orc_mensagem']);
  endif;
}
//Verifica a integridade dos dados e direciona as operações
private function CheckData() {
  if (in_array('', $this->Data)):
    $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
    $this->Result = false;
  elseif (!Check::Email($this->Data['orc_email'])):
    $this->Error = array("Digite um endereço de e-mail válido.", WS_ALERT, "Alerta!");
    $this->Result = false;
  else:
    $this->File = ( isset($this->Data['orc_anexo']) ? $this->Data['orc_anexo'] : null );
    $this->Data['orc_media'] = ( isset($this->Data['orc_media']) ? str_replace(',', '.', $this->Data['orc_media']) : null);
    $this->AmmountCart();
    $this->CheckFile();
  endif;
}
//Monta a tabela com os itens do carrinho
private function AmmountCart() {
  if (isset($_SESSION['CARRINHO']) && !empty($_SESSION['CARRINHO'])):
    $this->Data['orc_cart'] = null;
  foreach ($_SESSION['CARRINHO'] as $IDPRO => $PRODUCT):
    $produtos .= "<tr><td> Código: </td> <td> <strong> {$PRODUCT['prod_codigo']} </strong> </td></tr>";
    $produtos .= "<tr><td> Produto: </td> <td> <strong> {$PRODUCT['prod_title']} </strong> </td></tr>";
// $produtos .= "<tr><td> Valor unitário: </td> <td> <strong> R$ " . number_format($PRODUCT['prod_preco'], 2, ',', '.') . " </strong> </td></tr>";
    $produtos .= "<tr><td> <strong>Modelos:</strong> </td> <td> <strong>Quantidades:</strong> </td></tr>";
    foreach ($PRODUCT['modelos'] as $itens => $quantidade):
      $produtos .= "<tr><td> {$itens} </td> <td> <strong> {$quantidade} </strong> </td></tr>";
    endforeach;
    $produtos .= "<tr style='background: #eee; border-bottom:1px solid #ccc; padding: 2px; height:2px;'><td ></td><td></td></tr>";
  endforeach;
// $produtos .= "<tr><td>Valor total </td><td>R$ {$this->Total}</td></tr>";
  $this->Data['orc_cart'] = $produtos;
endif;
}
//Verifica e envia o anexo do cliente
private function CheckFile() {
  if (!empty($this->File)):
    $types = array(
      'image/jpg',
      'image/jpeg',
      'image/pjpeg',
      'image/png',
      'image/x-png'
    );
    if (in_array($this->File['type'], $types)):
      $true = true;
    else:
      $true = false;
    endif;
    if (!$true):
      $Upload = new Upload('doutor/uploads/');
      $FileName = "orcamento-{$this->Data['orc_name']}-" . (substr(md5(time() + $this->Data['orc_name']), 0, 10));
      $Upload->File($this->File, $FileName, EMPRESA_CLIENTE, 'orcamento', 2);
      if ($Upload->getError()):
        $this->Error = array($Upload->getError(), WS_ERROR, 'Alerta!');
        $this->Result = false;
      else:
        $this->Data['orc_anexo'] = $Upload->getResult();
        $this->File = 'doutor/uploads/' . $this->Data['orc_anexo'];
        $this->Result = true;
      endif;
    else:
      $Upload = new Upload('doutor/uploads/');
      $ImgName = "orcamento-{$this->Data['orc_name']}-" . (substr(md5(time() + $this->Data['orc_name']), 0, 10));
      $Upload->Image($this->Data['orc_anexo'], $ImgName, 1024, EMPRESA_CLIENTE, 'conta');
      if ($Upload->getError()):
        $this->Error = array($Upload->getError(), WS_ERROR, 'Alerta!');
        $this->Result = false;
      else:
        $this->Data['orc_anexo'] = $Upload->getResult();
        $this->File = 'doutor/uploads/' . $this->Data['orc_anexo'];
        $this->Result = true;
      endif;
    endif;
  else:
    $this->Result = true;
  endif;
}
//Cadastra os dados do relatorio no banco
private function Create() {
  $Create = new Create;
  $Create->ExeCreate(TB_ORCAMENTOS, $this->Data);
  if (!$Create->getResult()):
    $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
    $this->Result = false;
  else:
//$this->FTP['Id'] = $Create->getResult();
//$this->ConnectFTP();
    $this->Error = array("Orçamento enviado com sucesso.", WS_ACCEPT, "Aviso!");
    $this->Data = null;
    $this->File = null;
    $this->Total = null;
    $this->Result = true;
  endif;
}
//FUNÇÃO QUE ENVIA O JSON PARA O FTP DO CLIENTE
private function ConnectFTP() {
// Define se o servidor existe e faz a conexão
  $this->FTP['servidor'] = "ftp.seusite.com.br";
  $this->FTP['conexao'] = ftp_connect($this->FTP['servidor']);
  $this->FTP['usuario'] = "usuario";
  $this->FTP['senha'] = "senha";
// Acessa o FTP com usuário e senha caso conexão não retorne falso
  if ($this->FTP['conexao']):
    $this->FTP['ftp'] = ftp_login($this->FTP['conexao'], $this->FTP['usuario'], $this->FTP['senha']);
    $this->SendJson();
  endif;
}
//Envia o arquivo .json para o FTP.
private function SendJson() {
  if ($this->SetJson()):
// Envia o arquivo ao destino
    $this->FTP['envia'] = ftp_put($this->FTP['conexao'], $this->FTP['destinoFTP'] . $this->FTP['arquivoName'], $this->FTP['arquivoLocal'], FTP_BINARY);
    var_dump($this->FTP['envia']);
    if ($this->FTP['envia']):
      $this->DelJson();
    endif;
  endif;
// Fecha a conexão stream FTP
  ftp_close($this->FTP['conexao']);
}
private function SetJson() {
  $this->FTP['arquivoName'] = $this->FTP['Id'] . '-' . Check::Name($this->Data['orc_email']) . '.json';
  $this->FTP['destinoFTP'] = "public_html/rcv/";
  $this->FTP['arquivoLocal'] = 'doutor/uploads/JSon/' . $this->FTP['arquivoName'];
  if (!file_exists($this->FTP['arquivoLocal'])):
    $prodCod = null;
    $infos = null;
    $infos .= "{";
    $infos .= "'nome': '{$this->Data['orc_nome']}', 'email': '{$this->Data['orc_email']}', 'telefone': '{$this->Data['orc_telefone']}', 'cep': '{$this->Data['orc_cep']}', 'mensagem': '{$this->Data['orc_mensagem']}', 'produtos': [";
    foreach ($_SESSION['CARRINHO'] as $IDPRO => $PRODUCT):
      $prodCod .= "'{$PRODUCT['prod_codigo']}',";
    endforeach;
    $infos .= trim($prodCod, ',');
    $infos .= "]}";
    $this->FTP['Dados'] = fopen($this->FTP['arquivoLocal'], "w");
    fwrite($this->FTP['Dados'], str_replace("'", '"', $infos));
    fclose($this->FTP['Dados']);
    if (!file_exists($this->FTP['arquivoLocal']) && !is_dir($this->FTP['arquivoLocal'])):
      return false;
  else:
    return true;
  endif;
endif;
}
//Deleta o arquivo .json do nosso servidor
private function DelJson() {
//viabilizar se não formos manter backup.
}
}