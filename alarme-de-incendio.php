<? $h1 = "Alarme de incêndio";
$title  = "Alarme de incêndio";
$desc = "Receba uma cotação de $h1, encontre os melhores distribuidores, compare hoje com aproximadamente 500 fábricas ao mesmo tempo. É grátis!";
$key  = "Alarmes de incêndio,Comprar alarme de incêndio";
include('inc/head.php');
include('inc/fancy.php'); ?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main>
		<div class="content">
			<section>
				<?=$caminhoinformacoes?><br class="clear" />
				<h1><?=$h1?></h1>
				<article>
					<div class="img-mpi">
						<a href="<?=$url?>imagens/mpi/alarme-de-incendio-01.jpg" title="<?=$h1?>" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/alarme-de-incendio-01.jpg" title="<?=$h1?>" alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/alarme-de-incendio-02.jpg" title="Alarmes de incêndio" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/alarme-de-incendio-02.jpg" title="Alarmes de incêndio" alt="Alarmes de incêndio"></a><a href="<?=$url?>imagens/mpi/alarme-de-incendio-03.jpg" title="Comprar alarme de incêndio" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/alarme-de-incendio-03.jpg" title="Comprar alarme de incêndio" alt="Comprar alarme de incêndio"></a>
					</div>
					<span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span><hr />​
					<h2>CONHEÇA OS COMPONENTES DE UM ALARME DE INCÊNDIO</h2>
					<p>A ocorrência de um incêndio produz uma série de adversidades que afetam diversos aspectos. Poluição do ar atmosférico, redução da umidade relativa do ar, destruição de patrimônios, elevação de temperaturas da região, riscos de vida, complicações e desenvolvimento de problemas respiratórios, eliminação de determinadas plantas e animais... estas são algumas das fatalidades que podem decorrer com a ocorrência de um incêndio.</p>
					<p>Dependendo do material que estiver envolvido no desastre, o incêndio pode se alastrar de forma extremamente rápida e se tornar muito mais complicado de ser combatido, promovendo, dessa forma, inúmeros perigos para a saúde e segurança dos indivíduos que estão no local do acontecimento. Por isso muito se fala da prevenção de incêndios, pois trata-se da forma mais eficaz de impedir que eles se iniciem e que se desenvolvam em maiores dimensões.</p>
					<p>Todo prédio, seja ele comercial ou residencial, está sujeito a passar por acontecimentos adversos como incêndios, roubos e outros situações que envolvam algum tipo de pânico. A chance de ocorrer alguma outra circunstância desse tipo aumenta, ainda, caso se trate de uma edificação na qual existem muitos equipamentos eletrônicos, estoque de mercadorias e circulação constante de pessoas, sejam elas moradoras, visitantes, clientes, funcionários ou parceiros terceirizados.</p>
					<p>É importante que a segurança das pessoas que ali transitam, moram ou trabalham seja, o tempo todo, garantida, bem como o patrimônio em que se está. Para que isso seja uma realidade, é extremamente importante a instalação de um sistema de <strong>alarme de incêndio</strong>.</p>
					<p>Muitos incêndios podem ter início de formas imprevisíveis, como por exemplo, um curto circuito ou uma descarga elétrica. Para evitar que os danos causados possuam maiores dimensões, a existência de um <strong>alarme de incêndio</strong> é indicada para todas as edificações que possuam grandes dimensões e alto fluxo de pessoas.</p>
					<h2>QUAL A FUNÇÃO DO ALARME DE INCÊNDIO?</h2>
					<p>A função básica de um sistema de detecção e <strong>alarme de incêndio</strong>, assim como o próprio nome já revela, é conseguir realizar a detecção do fogo em seu estágio inicial, fazendo com que seja possível realizar a evacuação dos indivíduos ali presentes de forma mais ágil e segura possível, evitando acidentes graves, perdas no patrimônio e, sobretudo, riscos de vida.</p>
					<p>Os elementos básicos que compõem o sistema de alarme são a detecção, responsável pela parte do sistema que tem função de detectar o incêndio (detectores e acionadores manuais); o processamento de sinal, que envia os alertas até a central; e o aviso, que funciona por meio da central, ativando o sistema com as sinalizações sonoras e visuais. O aviso alerta as pessoas que estão presentes no ambiente em que está ocorrendo a situação de risco.</p>
					<h2>COMPONENTES DO ALARME DE INCÊNDIO</h2>
					<p>O sistema de detecção e <strong>alarme de incêndio</strong>, também conhecido pela sigla SADI, é formado por uma série de componentes interligados que, ao detectar qualquer sinal ou possibilidade de fumaça ou fogo, mesmo que mínimo, emitem notificações, colaborando para que se inicie a prevenção e o combate da situação.</p>
					<p>Alguns dos componentes que costumam estar presentes no sistema de detecção e alarme são:</p>
					<ul class="list">
						<li>Avisadores visuais e sonoros;</li>
						<li>Acionadores manuais;</li>
						<li>Detectores de fumaça;</li>
						<li><a target="_blank" href="<?=$url?>central-de-alarme">Central de alarme</a> e monitoramento.</li>
					</ul>
					<p>Cada um desses elementos devem ser posicionados de forma estratégica, a fim de atuarem com a melhor eficiência possível. Saiba mais sobre cada componente do <strong>alarme de incêndio</strong> de forma detalhada a seguir:</p>
					<h3>Detecção - Detector de fumaça</h3>
					<p>Os detectores de fumaça consistem em componentes importantes e que, por se tratarem de elementos de precisão, precisam ser fabricados em empresas de confiança e a sua instalação deve ser realizada por profissionais qualificados.</p>
					<p>Os detectores de fumaça são itens obrigatórios em ambientes como salas, quartos e outros locais que possuem ocupação temporária ou coletiva, como pensões, hotéis, motéis e asilos, por exemplo. Além disso, também devem estar presentes em áreas que são de uso comum em edificações, como corredores e halls de entrada. Locais que possam oferecer algum tipo de risco, como depósitos, estoques de mercadorias e com a presença de muitos equipamentos eletrônicos também necessitam da existência de detectores de fumaça.</p>
					<p>Apesar de existirem modelos variados, basicamente a ação deste tipo de componente é disparar o alerta assim que ele detectar algum tipo de indício de fumaça ou alteração de temperaturas causados pelo princípio de incêndio, enviando uma mensagem à central de <strong>alarme de incêndio</strong>.</p>
					<h3>Detecção - Acionadores manuais</h3>
					<p>Além dos detectores de fumaça e de temperatura, que criam um alerta para a central de <strong>alarme de incêndio</strong>, os acionadores manuais também enviam sinais de que existe algum tipo de indícios de incêndio ou que uma situação deste tipo realmente está ocorrendo. A diferença entre eles, no entanto, é que os detectores são acionados de forma automática, e a maioria dos acionadores, manualmente.</p>
					<p>Alguns exemplos de acionadores manuais são os botões de acionamento que possuem placas indicativas e o acionador do tipo "quebre o vidro". Como possuem acionamento manual, o ideal é que esses equipamentos sejam devidamente instalados em lugares que possuem trânsito recorrente de pessoas, como em corredores e outros lugares de uso comum.</p>
					<p>Uma ótima decisão é colocá-los na rota de fuga da edificação, próximo dos outros equipamentos de proteção contra incêndio, o que pode colaborar no momento de ocorrência de um incêndio. A instalação deve ser realizada em altura e local visível, pois os indivíduos presentes no local precisam conseguir dispará-los com a maior agilidade possível durante a situação de emergência.</p>
					<h3>Processamento de sinal - Central de alarme e monitoramento</h3>
					<p>É na central em que tudo acontece e, por isso, é considerada o "cérebro" da operação do <strong>alarme de incêndio</strong>. Todos os outros componentes do alarme estão conectados à ela, que possui como principal função receber as informações. Caso algum tipo de sinal de fumaça ou alteração anormal de temperatura ocorra, a central envia os comandos que ativam as sirenes de aviso do <strong>alarme de incêndio</strong>, sinalizando o perigo para as pessoas que ali estão.</p>
					<p>A central monitora todo o sistema de alarme e recebe, a todo momento, informações fornecidas pelos componentes. Além disso, ela também realiza a verificação constante da existência de possíveis falhas que possam afetar ou desregular o funcionamento do sistema de detecção e <strong>alarme de incêndio</strong>, como curtos circuitos e cabos rompidos, por exemplo.</p>
					<p>A grande maioria das centrais possuem um painel que sinaliza e informa a pessoa que acompanha seu funcionamento determinadas informações úteis, como a situação de comunicação dos componentes do <strong>alarme de incêndio</strong> e o nível de energia, por exemplo.</p>
					<p>É interessante que a central seja instalada em um local que possua vigilância constante para que se possa acompanhar o funcionamento da mesma. Portarias e guaritas de prédios, sejam eles comerciais, industriais ou residenciais e centrais de seguranças são alguns lugares comuns que costumam alocar as centrais de <strong>alarme de incêndio</strong>.</p>
					<h3>Aviso - Sinalizador sonoro, visual ou audiovisual</h3>
					<p>Estes componentes têm como função emitir sinais para indicar situação de emergência às pessoas que estão no ambiente. Quando o sistema de detecção e alarme é disparado, é de extrema importância que os indivíduos fiquem a par do que está acontecendo. Ao receberem a informação eles podem, então, buscar quais os procedimentos precisam realizar para evacuar o local e manterem-se sob proteção das chamas ou da situação de pânico existente.</p>
					<p>Abordando de forma mais específica o sinalizador visual, sabe-se que sua utilização deve ser realizada nos prédios em que possuem acesso de pessoas portadoras de deficiência auditiva e em fábricas que, por causa do alto ruído provocado pelas maquina, por exemplo, precisam utilizar protetores auriculares com certa frequência.</p>
					<p>Como atuam como verdadeiros avisos para as pessoas que estão nos prédios, é necessário que os sinalizadores sejam instalados em áreas que possuem trânsito intenso e constante de indivíduos e em locais que são de utilização comum dos funcionários ou moradores ou visitantes. Essa instalação é de grande importância, pois os sinalizadores tem como função alertar as pessoas, promovendo menos chances de que adversidades sérias venham a acontecer.</p>
					<h2>CENTRAL INTERLIGADA COM O CORPO DE BOMBEIROS</h2>
					<p>Dependendo da necessidade é possível, ainda, realizar diretamente uma interligação entre central de <strong>alarme de incêndio</strong> com a central de emergência do Corpo de Bombeiros mais próximo ao prédio. Essa interligação fica a critério do indivíduo responsável por arquitetar a edificação e pela coordenação do Corpo de Bombeiros.</p>
					<p>Caso ocorra uma situação de incêndio ou uma outra situação de pânico, a ligação existente colabora para que os bombeiros sejam avisados rapidamente e possa, assim, deslocarem-se até o lugar de ocorrência, minimizando as chances de complicações maiores.</p>
					<h2>AUTO DE VISTORIA DO CORPO DE BOMBEIROS</h2>
					<p>Tamanha a importância da prevenção contra incêndios que existem determinados tipos de normatizações que regulamentam normas e pré requisitos estabelecendo padrões de segurança que são necessárias para que o prédio seja considerado seguro, seja ele residencial, comercial ou de outra instância.</p>
					<p>Para que os estabelecimentos sejam considerados aptos a executarem suas funções, ou seja, sejam considerados seguros pelo Corpo de Bombeiros, existem exigências a serem seguidas, desenvolvidas e vistoriadas pelo próprio Corpo de Bombeiros, que é o órgão responsável por prevenir e combater incêndios. O cumprimento dessas normas de segurança e o respeito em relação às exigências são importantes para que um incêndio seja iniciado.</p>
					<p>O Auto de Vistoria do Corpo de Bombeiros, também conhecido como AVCB, comprova que a edificação está devidamente segura caso ocorra um incêndio ou outra situação de perigo ou pânico. Este certificado só pode ser emitido após o Corpo de Bombeiros realizar uma vistoria que atesta a segurança do local, permitindo que as atividades ali sejam desenvolvidas. Após a emissão do documento, é importante estar atento para que sua renovação seja realizada dentro do período de vencimento.</p>
					<p>Além da vistoria do AVCB devidamente realizada e renovada de acordo com as datas necessárias, indica-se a criação de uma brigada de combate de incêndios, a qual deve estar devidamente treinada para atuar em situações de pânico e, dessa forma, diminuir riscos de vida e perdas de patrimônio.</p>
					<p>O AVCB, além de ser extremamente importante para a segurança das pessoas que trabalham, visitam ou moram nos locais, também colabora em relação à preservação do patrimônio. Isso porque, caso ocorra um incêndio ou outro tipo de situação que acarrete em perdas patrimoniais e, no caso, o prédio possuir o AVCB atualizado, existem seguradoras que realizam o reembolso referente à perda para seus proprietários. A não existência de AVCB pode ocasionar no não recebimento do seguro.</p>
					<h2>EQUIPAMENTOS DE COMBATE A INCÊNDIO</h2>
					<p>Apesar do sistema de detecção e <strong>alarme de incêndio</strong> demonstrar eficiência, existem outros equipamentos que devem ser instalados nas edificações, a fim de proteger ainda mais as vidas que ali estão e oferecer ainda mais segurança para as mesmas. Cada um desses equipamentos, bem como suas instalações, possuem importância para que consigam ser utilizados com agilidade, eficiência e, dessa forma, colaborem prevenindo e combatendo incêndios e outras situações emergenciais. Alguns deles são:</p>
					<p><strong>Extintores:</strong> é um dos equipamentos de combate a incêndio mais comuns. Os extintores são classificados em tipos referentes às situações de incêndio em que devem ser utilizados. Alguns tipos são: extintor H2O, cuja formação é composta de água líquida; extintor pó químico, composto por bicarbonato de sódio; extintor à base de espuma; extintor de gases, composto por gás carbônico ou nitrogênio ou por vapor de água.</p>
					<p><strong>Sinalizações emergenciais:</strong> são as placas que indicam rotas de fuga e que sinalizam a localização de extintores, por exemplo. As placas de rota de fuga colaboram para que as pessoas possam realizar a evacuação do local em situações emergenciais.</p>
					<p><strong>Portas corta-fogo:</strong> geralmente presentes em saídas de escadas de pavimentos, as portas corta-fogo, assim como o próprio nome revela, são resistentes  ao fogo e à fumaça decorrente de incêndios. Este tipo de porta costuma ser constituído por aço galvanizado, possuem alta resistência ao fogo e também apresentam alto nível de isolamento térmico. Essas portas facilitam na rota de fuga em casos de situações de emergência.</p>
					<p><strong>Iluminações de emergência:</strong> caso aconteça uma queda de energia, as iluminações emergenciais colaboram fornecendo iluminação necessária para que as pessoas consigam evacuar o prédio. Geralmente este tipo de equipamento é mais comum em edificações que têm mais de dois pavimentos ou que possuem trânsito intenso e constante de pessoas.</p>
				</article>
				<? include('inc/coluna-mpi.php');?>
				<br class="clear">
				<? include('inc/busca-mpi.php');?>
				<? include('inc/form-mpi.php');?>
				<? include('inc/regioes.php');?>
			</section>
		</div>
	</main>
</div>
<? include('inc/footer.php');?>
</body>
</html>