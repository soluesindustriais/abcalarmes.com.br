<? $h1 = "Central de alarme";
$title  = "Central de alarme";
$desc = "Se procura por $h1, conheça as melhores indústrias, cote produtos hoje com mais de 200 fabricantes. É rápido, fácil e gratuito! Confira!";
$key  = "Centrais de alarmes,Preço de central de alarme";
include('inc/head.php');
include('inc/fancy.php'); ?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main>
		<div class="content">
			<section>
				<?=$caminhoinformacoes?><br class="clear" />
				<h1><?=$h1?></h1>
				<article>
					<div class="img-mpi">
						<a href="<?=$url?>imagens/mpi/central-de-alarme-01.jpg" title="<?=$h1?>" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/central-de-alarme-01.jpg" title="<?=$h1?>" alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/central-de-alarme-02.jpg" title="Centrais de alarmes" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/central-de-alarme-02.jpg" title="Centrais de alarmes" alt="Centrais de alarmes"></a><a href="<?=$url?>imagens/mpi/central-de-alarme-03.jpg" title="Preço de central de alarme" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/central-de-alarme-03.jpg" title="Preço de central de alarme" alt="Preço de central de alarme"></a>
					</div>
					<span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span><hr />
					<h2>CENTRAL DE ALARME:  CONHEÇA COMO TER MAIS SEGURANÇA</h2>
					<p>Todas as edificações devem seguir as normas de segurança contra incêndio, pois sõ essenciais para diminuir riscos de incêndio e de acidentes no local. As edificações residenciais também devem seguir essas exigências, exceto imóveis unifamiliares, ou seja, que são constituídos por um única família, caso tenha mais de uma família como acontece em condomínios residenciais é necessário adotar as medidas previstas.</p>
					<p>A normativa foi desenvolvida pelo Corpo de Bombeiros a fim de que todos os estabelecimentos cumpram e tenham  emissão do certificado de vistoria do Corpo de Bombeiros que tem como objetivo formalizar a regularidade de determinado imóvel.</p>
					<h2>COMO CONSEGUIR APROVAÇÃO DO CORPO DE BOMBEIROS?</h2>
					<p>Antes da obra ser iniciada é necessário que o profissional técnico faça todo o levantamento da normativa e exigências que devem ser cumpridas para determinado tipo de edificação. O primeiro passo é a elaboração de um projeto de incêndio que contenha as informaçõe básicas como metragem, tipos de equipamentos a serem instalados, quantidade, entre outras especificações.</p>
					<h2>TRÂMITE PARA APROVAÇÃO DO PROJETO</h2>
					<p>Após todo o levantamento de norma e estratégia de instalação dos equipamentos, é necessário que um profissional técnico providencie a elaboração do projeto e assim que estiver pronto é necessário protocolar junto ao Corpo e bombeiros para  que o órgão faça a análise.</p>
					<p>O bombeiro irá analisar se o projeto atende as exigências para determinado tipo de imóvel, por isso, é importante fazer todo o levantamento antes de executar o projeto seguindo todas as exigências descritas nas normas, para que tenha maior chances de ser aprovado quando passar por essa avaliação técnica.</p>
					<p>Assim que análise for finalizada o requerente será sinalizado por email ou por um sistema, a forma do andamento do processo varia para cada município, por isso é importante perguntar no ato o protocolo como os processos são conduzidos naquela região.</p>
					<p>Após análise, o projeto poderá ser aprovado ou não, se aprovado deverá seguir para execução e se for reprovado, o bombeiro costumam emitir um relatório com todos os itens a serem adequados esse procedimento se prete até que o projeto seja aprovado.</p>
					<h2>OBTENÇÃO DO CERTIFICADO</h2>
					<p>Quando o projeto é aprovado, a obra é liberada para execução, porém é necessário que os serviços sejam executados exatamente da forma em que o projeto foi elaborado e aprovado, considerando todas as informações.</p>
					<p>Assim que obra for finalizada, é necessário que solicitem  vistoria para o Corpo de bombeiros, que em alguns municípios têm até trinta dias para vistoriar o local solicitado. Na vistoria os bombeiros observam se os itens foram instalados de acordo com o projeto, anteriormente aprovado, se houver alguma divergência a vistoria é reprovada e uma notificação é emitida com todas as adequações necessárias.</p>
					<p>O responsável papel obra deverá regularizar o imóvel até que atenda as exigências e a vistoria seja aprovada. Assim que os bombeiros aprovarem, o certificado pode ser emitido, porém em alguns municípios essa emissão não é feita imediatamente, podendo levar alguns dias ou semana para estar disponível.</p>
					<h2>EMISSÃO DO CERTIFICADO</h2>
					<p>O certificado é emitido e nele conta algumas informações sobre o imóvel como metragem, endereço, tipo de segmento, atividades exercidas, além da data de validade. Cada município atribui um modelo, porém normalmente este documento possui essas informações.</p>
					<p>É necessário estar atento a data de vencimento do certificado podendo renovar a aprovação dois meses antes de vencer. A validade também varia existem municípios que o documento tem validade de um, dois ou cinco anos. O Corpo de bombeiros é um órgão que estabelece algumas particularidades para cada região, por isso, é importante estar atento a essas questões.</p>
					<p>Além disso, o atendimento com o Corpo de Bombeiros pode ser feito de formas diferentes, para cada localidade, pois possuem atendimentos e prazo diferenciados. Alguns oferecem atendimento online, outros somente presencial, por isso, é importante verificar quais são os procedimento de aprovação com o Corpo de bombeiro local.</p>
					<h2>RISCOS DE TER A DOCUMENTAÇÃO VENCIDA OU NÃO EMITIDA?</h2>
					<p>A certificação é extremamente importante, pois é o documento que atesta o funcionamento adequado do imóvel para os itens de segurança contra incêndio. As vistorias dos bombeiros a fim de identificar irregularidades principalmente em grandes imóveis com fins comerciais tem sido frequente, por esse motivo é essencial que o proprietário mantenha  edificação sempre regular, pois essas vistorias não são comunicadas.</p>
					<p>Se houver alguma ocorrência que ofereça risco ou reclamação de algum cliente, também poderá ser um fator de alerta ao proprietário, pois se não tiver a certificação poderá ter o estabelecimento interditado ou poderá receber multas por  não cumprir com as obrigações  de segurança.</p>
					<p>A certificação não determina necessariamente o funcionamento constante dos equipamentos de incêndio, mas sim que o imóvel possui as condições básicas de segurança com equipamentos de incêndio suficientes para combater o fogo caso ocorra algum incêndio no local, mas quanto ao funcionamento do equipamento é necessário que o proprietário e o responsável técnico garanta com a frequência das manutenções preventivas.</p>
					<h2>QUAL A FUNÇÃO DOS EQUIPAMENTOS DE INCÊNDIO?</h2>
					<p>São itens indispensáveis para promover a segurança de determinado local, pois além de combater conseguem sinaliza quando alguma situação de risco ocorre. A seguir alguns equipamentos e suas características e funções:</p>
					<p><strong>Hidrantes:</strong> é uma tubulação que permite maior saída de água para o combate ao fogo e possui uma mangueira bem resistente capaz de suportar a pressão da água;</p>
					<p><strong>Porta corta-fogo:</strong> é uma porta com maior resistência que impede que o fogo e a fumaça passe de um ambiente para outro;</p>
					<p><strong>Placas de sinalização:</strong> são indicações, orientações e proibições voltadas para a segurança;</p>
					<p><strong>Rota de fuga:</strong> é um trajeto que direciona as pessoas estrategicamente para a evacuação mais segura e fácil;</p>
					<p><strong>Extintores:</strong> possui o formato de um cilindro e quando pressionado emite substâncias capazes de cessar o fogo, como á água, gás carbônico, pó químico.</p>
					<p><strong>Central de alarme</strong>: um sistema constituído por sensores que identificam a fumaça e acionam a sirene para alertar e provocar a evacuação o imóvel;</p>
					<p><strong>Sprinkler:</strong> caracterisiticas de um chuveiro e quando seu vidro é exposto a altas temperaturas quebra e elimina água para combater as chamas.</p>
					<ul class="list">
						<li><strong>Sprinkler:</strong> Parece com um chuveiro e quando seu sensor é acionado por uma determinada quantidade de fumaça libera água a fim de combater o fogo;</li>
						<li><a target="_blank" href="<?=$url?>central-de-alarme-de-incendio">Central de alarme de incêndio</a>: São sensores e ficam espalhados, quando acionados por determinada quantidade de fumaça são acionados e emitem um barulho com a intenção de fazer as pessoas evacuarem do ambiente;</li>
						<li><strong>Extintores:</strong> É um cilindro com composições químicas e quando esses gases são submetidos a pressão causada pela válvula do cilindro conseguem combater o incêndio.</li>
						<li><strong>Rota de fuga:</strong> Não é um equipamento, mas trata-se de um trajeto simples e desobstruído para facilitar a evacuação em casos de risco;</li>
						<li><strong>Placas de sinalização:</strong> São diversos modelos e com orientações diferentes, servem para indicar um produto ou um lugar, geralmente são fotoluminescentes para facilitar a leitura quando houver falta de iluminação;</li>
						<li><strong>Hidrantes:</strong> Uma tubulação com mangueira resistente e extensa para a saída de água com maior pressão;</li>
						<li><strong>Porta corta-fogo:</strong> É resistente ao fogo e impede o mesmo e a fumaça passe de um espaço para o outro.</li>
					</ul>
					<p>Existem muitos outros equipamentos de incêndio que podem oferecer maior segurança para um ambiente, fazendo com que os riscos de incêndio e acidente diminuam e se houver algum sinistro viabilizar o controle da situação proporcionando maior chances de sair ileso a situações de emergência.</p>
					<p>É importante também que as pessoas sejam orientadas e conduzidas, pois nada adianta os equipamento funcionarem adequadamente e as pessoas não saberem como agir nesse tipo de caso. Por exemplo, no caso de quando a sirene da <strong>central de alarme</strong> disparar, esse é um aviso para que as pessoas evacuem do local, por isso, ´imprescindível que saibam como agir no suposto incêndio.</p>
					<h2>COMO FUNCIONA O SISTEMA DA CENTRAL DE ALARME?</h2>
					<p>É um sistema de funcionamento simplificado onde são instalados detectores de fumaça que são interligados a uma <strong>central de alarme</strong>. Os detectores são sensíveis a fumaça e quando há indicação no local, o comando aciona a <strong>central de alarme</strong> que dispara a sirene, avisando que há um  suposto incêndio no local. Esse sistema é constituído por dois tipos de sistema, o convencional e o sistema endereçável, havendo alguma diferença entre eles.</p>
					<h2>SISTEMA CONVENCIONAL DA CENTRAL DE ALARME</h2>
					<p>É um sistema mais indicado para ser instalado em áreas menores e com quantidade menor de dispositivos. A identificação usada para o detectores na <strong>central de alarme</strong> é conhecida como zona, ou seja, quando um detector aciona é informado na <strong>central de alarme</strong> o  local onde há suposto incêndio, porém não informa o dispositivo exato, apenas local como um todo.</p>
					<p>Sendo assim, é necessário procurar em qual área existem chamas, por esse motivo é recomendado para áreas menores, pois se for um imóvel de grande extensão pode perder muito tempo procurando enquanto o fogo se alastra para as demais áreas.</p>
					<h2>SISTEMA ENDEREÇÁVEL DA CENTRAL DE ALARME</h2>
					<p>Possui algumas funcionalidade mais modernas e esse modelo é mais indicado para áreas maiores. Para tipo de dispositivo, ou seja, detector existe uma numeração para identificação, portanto, quando acionam a sirene a aparece na central a numeração correspondente ao detector informando o local exato onde há presença de fogo. Essa facilidade é muito útil para localizar a área de forma mais rápida e conseguir combater melhor o fogo.</p>
					<p>Existem duas categorias que dividem esse sistema e são classificadas como A e B, o que permite e oferece duas versões e possibilidades para o mesmo sistema a fim o cliente identificar qual melhor o atende.</p>
					<p><strong>Categoria A:</strong> para que o dispositivo esteja interligado a central, é necessário que o cabeamento faça essa interligação passando por toda  estrutura de alvenaria. Com isso, faz uso de maior quantidade de cabeamento, porém como vantagem dispara somente o dispositivo alarmado e não impede o funcionamento dos demais.</p>
					<p><strong>Categoria B:</strong> esse método possui apenas um cabeamento e é mais econômico, porém quando um dispositivo é acionado o funcionamento dos demais sofre interferência prejudicando a função dos demais.</p>
					<p>Ambas categorias da <strong>central de alarme</strong> apresentam vantagem e desvantagem, porém é preciso identificar qual melhor atende determinado ambiente e objetivo. O sistema endereçável é mais moderno também esteticamente, pois seus dispositivos são mais agradáveis visualmente. Esse dispositivo não pode ser totalmente embutido, pois interfere na funcionalidade do equipamento, pois é necessário fique aparente para captar melhor a fumaça, sendo assim, para manter um aspeto moderno o sistema endereçável tem recebido forte investimento.</p>
					<h2>MANUTENÇÃO É FUNDAMENTAL</h2>
					<p>A manutenção deve ser indispensável para qualquer item de incêndio, pois consegue prevenir falhas na operação e garantir que o sistema esteja em perfeito funcionamento em casos de sinistro. Mesmo que a edificação tenha o certificado emitido, é importante que a manutenção preventiva seja feita na <strong>central de alarme</strong> e outros itens de segurança.</p>
					<p>É necessário a contratação de um profissional técnico para acompanhar e no caso da <strong>central de alarme</strong> o técnico usa uma espécie de spray para testar os acionadores, a fim de verificar se vão disparar, pois imitam as reações a fumaça. Esse procedimento serve para ver se o dispositivo está funcionando e se está acionando a sirene e a <strong>central de alarme</strong>, com isso, é possível identificar falhas  no sistema e providenciar a manutenção ou até mesmo a substituição das peças.</p>
					<h2>ONDE ENCONTRAR?</h2>
					<p>Existem muitas empresas que fornecem a <strong>central de alarme</strong> e outros materiais de incêndio e também o serviço de manutenção preventiva, porém é importante se atentar para contratar uma empresa de confiança, pois por se tratar de equipamentos de segurança é preciso garantir que seja uma empresa que ofereça qualidade e eficiência.</p>
					<p>São muitas variedades de produtos que podem ser encontrados também na internet, inclusive no portal o Soluçõe Industriais que centraliza empresas no segmento de segurança contra incêndio. É importante fazer uma vasta pesquisa de qual empresa pode oferecer o melhor serviço.</p>
					<h2>A SEGURANÇA É PRIMORDIAL</h2>
					<p>É fundamental que o sistema de <strong>central de alarme</strong> e outros sistemas seja instalado com eficiência e seguindo as normas para que diminua as chances de existir falhas no funcionamento do sistema.</p>
					<p>Devido ser um item de segurança é necessário que receba atenção e acompanhamento de uma empresa que ofereça qualidade. Além disso, para as empresas é possível que palestras, treinamentos sejam aplicados para os colaboradores a fim de contribuir para a segurança de todos.</p>
				</article>
				<? include('inc/coluna-mpi.php');?>
				<br class="clear">
				<? include('inc/busca-mpi.php');?>
				<? include('inc/form-mpi.php');?>
				<? include('inc/regioes.php');?>
			</section>
		</div>
	</main>
</div>
<? include('inc/footer.php');?>
</body>
</html>